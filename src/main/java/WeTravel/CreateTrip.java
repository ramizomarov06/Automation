package WeTravel;


import org.openqa.selenium.WebDriver;


import java.time.Duration;

public class CreateTrip {
    //private final WebDriver driver;

//    public CreateTrip(WebDriver driver) {
//        this.driver = driver;
//    }
    public boolean perform(WebDriver driver) throws InterruptedException {
        CoreData CreateTrip = new CoreData(driver);
        CreateTrip.myMethod(2000);
        CreateTrip.getCreateYourTrip().click();
        CreateTrip.myMethod(2000);
        CreateTrip.getInputDestination().sendKeys("Торонто, Онтарио, Канада");
        CreateTrip.myMethod(2000);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        CreateTrip.getNextStep().click();
        CreateTrip.getStartDateInput().click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        CreateTrip.setStartDateInput().click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        CreateTrip.getEndDateInput().click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        CreateTrip.setStartDateInput().click();
        CreateTrip.getNextButtonSchedule().click();
        CreateTrip.getPrivateClickLabel().click();
        CreateTrip.getSeeTripNextClick().click();
        return true;
    }
}
