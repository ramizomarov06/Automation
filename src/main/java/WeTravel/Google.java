package WeTravel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;


import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Map;

import static java.lang.System.out;

public class Google {
    //public static void main(String[] args) throws MalformedURLException {
    public static void main(String[] args ) throws Exception {

        ChromeOptions options = new ChromeOptions();
        options.setCapability("browserName", "chrome");
        options.setCapability("browserVersion", "111.0");
        options.setCapability("selenoid:options", Map.of(
                "enableVNC", true,
                "screenResolution", "1280x720x24"
        ));
        RemoteWebDriver driver = new RemoteWebDriver(
                new URL("http://144.91.94.98:4444/wd/hub"),
                options
        );
        driver.get(CoreData.getGooglePage());
        out.println(driver.getTitle());
        //driver.quit();

    }
}