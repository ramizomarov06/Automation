package WeTravel;


import org.openqa.selenium.WebDriver;

public class TripDescription {

    public boolean perform(WebDriver driver) throws InterruptedException{
        CoreData TripDescription = new CoreData(driver);
        TripDescription.myMethod(2000);
        TripDescription.getTripDescriptionAbout().sendKeys("General Information");
        TripDescription.addCustomParagraph().click();
        TripDescription.setParagraphTitle().sendKeys("Minor Information");
        TripDescription.myMethod(2000);
        TripDescription.setCustomParagraphDescription().click();
        TripDescription.setCustomParagraphDescription().sendKeys("About Minor Information");
        TripDescription.myMethod(2000);
        TripDescription.addIncludedItem().click();
        TripDescription.setIncludedItem().click();
        TripDescription.setIncludedItem().sendKeys("Kabab");
        TripDescription.myMethod(2000);
        TripDescription.setIncludedItemDescription().click();
        TripDescription.setIncludedItemDescription().sendKeys("Some food like kabab");
        TripDescription.myMethod(2000);
        TripDescription.addNotIncludedItem().click();
        TripDescription.myMethod(2000);
        TripDescription.setNotIncludedItem().click();
        TripDescription.setNotIncludedItem().sendKeys("Nothing");
        TripDescription.myMethod(2000);
        TripDescription.setNotIncludedItemDescription().click();
        TripDescription.setNotIncludedItemDescription().sendKeys("Nothing else");
        TripDescription.myMethod(2000);
        //TripDescription.clickSavedDraft().click();
        TripDescription.getNextButtonTrip().click();

        return true;
    }
}
