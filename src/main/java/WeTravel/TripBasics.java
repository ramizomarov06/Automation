package WeTravel;

//import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Core;
import org.openqa.selenium.WebDriver;

import javax.swing.tree.TreePath;
import java.time.Duration;



public class TripBasics {
   // private final WebDriver driver;

    //public TripBasics(WebDriver driver) {
      //  this.driver = driver;
    //}
    public boolean perform(WebDriver driver) throws InterruptedException {
        CoreData TripBasics = new CoreData(driver);

        TripBasics.getTripTitle().sendKeys("AllIneed");
        TripBasics.getTripOffered().click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        TripBasics.setSelectOffer().click();
        TripBasics.getTripOffered().click();
        TripBasics.getSearchPhoto().click();
        TripBasics.myMethod(2000);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        TripBasics.setSearchPhoto().sendKeys("Cats");
        TripBasics.setSearchPhotoButton().click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        TripBasics.setSearchPhotoElement().click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        TripBasics.getViewSelectedPhoto().click();
        TripBasics.myMethod(2000);
        TripBasics.getSelectedUploadPhoto().click();
        TripBasics.myMethod(10000);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        TripBasics.getGroupMinimum().click();
        TripBasics.myMethod(2000);
        TripBasics.setGroupMinimum().click();
        TripBasics.myMethod(2000);
        TripBasics.getGroupMaximum().click();
        TripBasics.myMethod(2000);
        TripBasics.setGroupMaximum().click();
        TripBasics.getPrivateCurrentClick().click();
        TripBasics.myMethod(2000);
        TripBasics.getNextButtonTrip().click();

        return true;

    }
}
