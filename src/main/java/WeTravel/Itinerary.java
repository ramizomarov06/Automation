package WeTravel;

import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class Itinerary {
    public boolean perform(WebDriver driver) throws InterruptedException{
        CoreData Itinerary = new CoreData(driver);
        Itinerary.myMethod(2000);
        Itinerary.clickEditItinerary().click();
        Itinerary.getSearchPhoto().click();
        Itinerary.myMethod(2000);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        Itinerary.setSearchPhoto().sendKeys("Cats");
        Itinerary.setSearchPhotoButton().click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        Itinerary.setSearchPhotoElement().click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        Itinerary.getViewSelectedPhoto().click();
        Itinerary.myMethod(2000);
        Itinerary.setEventTitle().sendKeys("10 Days for NewYork");
        Itinerary.myMethod(2000);
        Itinerary.setEventName().sendKeys("Try to see best place");
        Itinerary.clickSaveEvent().click();
        Itinerary.myMethod(1000);
        Itinerary.getNextButtonTrip().click();


        return true;

    }
}
