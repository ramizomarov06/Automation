package com.tests;

import WeTravel.*;
import io.qameta.allure.Step;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Map;

public class StepStartTest {
    private WebDriver driver;

    public StepStartTest(String ignoredCurrentWindow) {
    }

    @BeforeClass
    @Parameters({"server_linode"})
    public void setupDriver(String server_linode) throws MalformedURLException {

        // Create ChromeOptions object
        ChromeOptions options = new ChromeOptions();
        options.setCapability("browserName", "chrome");
        options.setCapability("browserVersion", "111.0");
        options.setCapability("selenoid:options", Map.of(
                "enableVNC", true,
                "screenResolution", "1920x1080x24"
        ));
        //это под selenium/standalone
        //options.addArguments("--remote-allow-origins=*");
        // Set the window size
        options.addArguments("start-maximized");
        //options.addArguments("--disable-dev-shm-usage");
        // Create a new instance of the ChromeDriver
        //driver = new ChromeDriver(options);
        driver = new RemoteWebDriver(new URL(server_linode), options);
        CoreData Google = new CoreData(driver);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    }

    @Test
    @Parameters({"_fin"})
    public void firstStepLoginTest(String _fin) throws InterruptedException {
        //simpleGooglePage();
        weTravelLoginPage(_fin);


    }
    @Test (dependsOnMethods = {"firstStepLoginTest"})
    public void secondStepCreateTripTest() throws InterruptedException{
        createTripStart();

    }
    @Test (dependsOnMethods = {"secondStepCreateTripTest"})
    public void thrirdStepTripBasicTest() throws InterruptedException{
        createTripBasics();

    }
    @Test (dependsOnMethods = {"thrirdStepTripBasicTest"})
    public void fourthStepTripDescTest() throws InterruptedException{
        descriptionData();
    }
    @Test (dependsOnMethods = {"fourthStepTripDescTest"})
    public void fifthStepItinerary() throws InterruptedException{
        createItinerary();
    }


    @Step
    public void weTravelLoginPage(String _fin) throws InterruptedException {
        LogIn logIn = new LogIn();
        Assert.assertTrue(logIn.perform(driver, _fin));
    }

    @Step
    public void createTripBasics() throws InterruptedException {
        TripBasics nextPage = new TripBasics();
        Assert.assertTrue(nextPage.perform(driver));

    }
    @Step
    public void createTripStart() throws InterruptedException{
        CreateTrip tripStart = new CreateTrip();
        Assert.assertTrue(tripStart.perform(driver));
    }
    @Step
    public void descriptionData() throws InterruptedException{
        TripDescription descData = new TripDescription();
        Assert.assertTrue(descData.perform(driver));
    }
    @Step
    public void createItinerary() throws InterruptedException{
        Itinerary dataItinenary = new Itinerary();
        Assert.assertTrue(dataItinenary.perform(driver));
    }


    @AfterTest
    public void closeDriver(){
        //driver.quit();
    }

}
